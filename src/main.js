import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "bulma/css/bulma.css";
import "../src/assets/fontawesome-free-5.13.0-web/css/all.css";

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
